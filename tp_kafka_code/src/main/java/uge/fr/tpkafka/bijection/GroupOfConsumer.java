package uge.fr.tpkafka.bijection;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

public class GroupOfConsumer {
    static HashMap<String, Double> map = new HashMap<String, Double>();
    // slave function that takes a Map of PharmacyId as key and a value that is added to List of prices
    public static HashMap<String, Double> addPriceAccordingToCIP(HashMap<String, Double> map, String cip, double price) {
        if (map.containsKey(cip)) {
            map.put(cip, map.get(cip) + price);
        } else {
            map.put(cip, price);
        }
        return map;
    }
    public static void main(String[] args) throws InterruptedException {
        String newtopic = "Top2";
        String groupId = "mygroup";
        String schemaString = "{\"type\":\"record\",\"name\":\"RecordGenerator\",\"fields\":[{\"name\":\"firstName\",\"type\":\"string\"},{\"name\":\"lastName\",\"type\":\"string\"},{\"name\":\"cip\",\"type\":\"int\"},{\"name\":\"price\",\"type\":\"double\"},{\"name\":\"pharmacyId\",\"type\":\"int\"}]}";
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092,localhost:9093,localhost:9094");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        props.put("group.id", groupId);

        Injection<GenericRecord, byte[]> injection = GenericAvroCodecs.toBinary(new org.apache.avro.Schema.Parser().parse(schemaString));

        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(props);

        consumer.assignment().forEach(System.out::println);
        consumer.subscribe(Collections.singletonList(newtopic));
        while (true) {
            System.out.println("Polling...");
            ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(1000));
            for (ConsumerRecord<String, byte[]> record : records) {
                GenericRecord avroRecord = injection.invert(record.value()).get();
                int cip = (int) avroRecord.get("cip");
                double price = (double) avroRecord.get("price");
                var key = record.key();

                // print consumer id and message
                addPriceAccordingToCIP(map, key, price);
                System.out.println("Updating ... ");

                //Every time a new record was received we add the key  to the map, and update value display the map
                for (var keyTopic : map.keySet()) {
                    System.out.println("PARTITION [" +record.partition()+ "] Key(CIP):" + keyTopic + ", total recipe : " + map.get(keyTopic)+"€");
                }
            }
        }
//                Set<TopicPartition> partitions = records.partitions();
//                for (TopicPartition partition : partitions) {
//                    System.out.println("Partition: " + partition + " - Percentage consumed: " + (records.count() / partition.partition() * 100) + "%");
//                }


    }

}
