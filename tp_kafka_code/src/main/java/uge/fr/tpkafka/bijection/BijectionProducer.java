package uge.fr.tpkafka.bijection;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import uge.fr.tpkafka.services.PostgresService;
import uge.fr.tpkafka.RecordGenerator;

import java.sql.SQLException;
import java.util.Properties;

public class BijectionProducer {

    public static void main(String[] args) throws SQLException, InterruptedException {
        String topic = "mytopic";
        String schemaString = "{\"type\":\"record\",\"name\":\"RecordGenerator\",\"fields\":[{\"name\":\"firstName\",\"type\":\"string\"},{\"name\":\"lastName\",\"type\":\"string\"},{\"name\":\"cip\",\"type\":\"int\"},{\"name\":\"price\",\"type\":\"double\"},{\"name\":\"pharmacyId\",\"type\":\"int\"}]}";
        var schema = new Schema.Parser().parse(schemaString);
        // Create the injection
        Injection<GenericRecord, byte[]> injection = GenericAvroCodecs.toBinary(schema);
        // Create the producer properties
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092, localhost:9093, localhost:9094");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArraySerializer");


        // Create the producer
        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(props);
        System.out.println("Producer created");
        // Create the Avro recordd
        GenericRecord avroRecord = new GenericData.Record(schema);
        var bd = new PostgresService();
        for (int i = 0; i<1500; i++){
            Thread.sleep(1);
            var myRecord = new RecordGenerator(bd);
            avroRecord.put("firstName", myRecord.name);
            avroRecord.put("lastName", myRecord.lastName);
            avroRecord.put("cip", myRecord.cip);
            avroRecord.put("price", myRecord.price);
            avroRecord.put("pharmacyId", myRecord.pharmacyId);
            // Serialize the Avro record

            byte[] serializedRecord = injection.apply(avroRecord);
            // Create the producer record
            ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, serializedRecord);
            // Send the record
            producer.send(record);
            System.out.println("Record "+ i+" sent successfully");
        }
        bd.close();
    }
}