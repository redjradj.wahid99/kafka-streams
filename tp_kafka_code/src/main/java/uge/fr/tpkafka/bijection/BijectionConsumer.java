package uge.fr.tpkafka.bijection;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class BijectionConsumer {
    public static void main(String[] args) throws InterruptedException {
        String topic = "mytopic";
        String newTopic = "Top2";
        String groupId = "mygroup";
        String schemaString = "{\"type\":\"record\",\"name\":\"RecordGenerator\",\"fields\":[{\"name\":\"firstName\",\"type\":\"string\"},{\"name\":\"lastName\",\"type\":\"string\"},{\"name\":\"cip\",\"type\":\"int\"},{\"name\":\"price\",\"type\":\"double\"},{\"name\":\"pharmacyId\",\"type\":\"int\"}]}";

        // Create the injection
        Injection<GenericRecord, byte[]> injection = GenericAvroCodecs.toBinary(new org.apache.avro.Schema.Parser().parse(schemaString));

        // Create simple consumer properties
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092, localhost:9093, localhost:9094");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(props);

        // Configure Producer properties in order to produce in 3 partitions and 3 replicas (3 brokers)
        Properties props2 = new Properties();
        props2.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092,localhost:9093,localhost:9094");
        props2.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props2.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(props2);

        consumer.subscribe(Collections.singletonList(topic));
        boolean running = true;
        Duration oneSecond = Duration.ofSeconds(1);
        while (running) {

                //Thread.sleep(1);
                System.out.println("Polling...");
                var records = consumer.poll(oneSecond);
               //Once we have the records, we can iterate over them and produce in the new topic
                for (var record : records) {
                    // Deserialize the Avro record
                    var avroRecord = injection.invert(record.value()).get();
                    var firstName = avroRecord.get("firstName").toString();
                    var lastName = avroRecord.get("lastName").toString();
                    int cip = (int) avroRecord.get("cip");
                    double price = (double) avroRecord.get("price");
                    int pharmacyId = (int) avroRecord.get("pharmacyId");

                    System.out.println("Record received: " + firstName + " " + lastName + " " + cip + " " + price + " " + pharmacyId);
                    //Ici il faut crée un producer V2 avec une clé qui va être le cip
                    //et une valeur qui va être le prix et le renvoyé dans le topic mytopic2
                    ProducerRecord<String, byte[]> recordForTopic2 = new ProducerRecord<>(newTopic, String.valueOf(cip), injection.apply(avroRecord));
                    // On envoi le record, pour chaque cip
                    producer.send(recordForTopic2);
                    System.out.println("Record sent to "+recordForTopic2.topic()+ ",  " + cip + ", " + price + ", " + pharmacyId);
                }
            }

        }

    }





