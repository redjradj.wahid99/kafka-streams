package uge.fr.tpkafka.kstreams;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import uge.fr.tpkafka.RecordGenerator;


import java.util.Properties;

public class MyKStreams {

    public static void main(String[] args) {
        String topic = "mytopic";
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "my-streams-app");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "mygroup");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
//        props.put("default.key.serde", "org.apache.kafka.common.serialization.Serdes$StringSerde");
//        props.put("default.value.serde", "org.apache.kafka.common.serialization.Serdes$StringSerde");

        StreamsConfig config = new StreamsConfig(props);
        // Version 2, Using topology
//        var bd = new PostgresService();
//        Topology topology = new Topology();
//        topology.addSource("mySource", topic);
//        topology.addProcessor("myProcessor", MyProcessor::new, "mySource");
//        topology.addSink("mySink", "topic-anonymous", "myProcessor");

        StreamsBuilder builder = new StreamsBuilder();
        var gson = new Gson();
        KStream<String, String> stream = builder.stream(topic, Consumed.with(Serdes.String(), Serdes.String()));
        KStream<String, String> anonymousStream = stream.mapValues( value -> {
            RecordGenerator jsonObject = gson.fromJson(value, RecordGenerator.class);
            System.out.println("Anonymizing record .... [cip: " + jsonObject.cip + "]");
            jsonObject.setName("***");
            jsonObject.setLastName("***");
            System.out.println("Sending anonymized record to [topic-anonymous] ....");
            System.out.println("-----------------------");

            return RecordGenerator.getJsonObject(jsonObject);
        }).filter((key, value) -> gson.fromJson(value, RecordGenerator.class).price >= 4);
        anonymousStream.to("topic-anonymous", Produced.with(Serdes.String(), Serdes.String()));
        final KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), config);
        kafkaStreams.start();

//        kafkaStreams.close();
    }
}
