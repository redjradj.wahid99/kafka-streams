package uge.fr.tpkafka.kstreams;

import com.google.gson.Gson;
import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.ProcessorContext;
import org.apache.kafka.streams.processor.api.Record;
import uge.fr.tpkafka.RecordGenerator;

public class MyProcessor implements Processor<Void, String, Void, String> {
    private ProcessorContext<Void, String> context;

    @Override
    public void init(ProcessorContext<Void, String> context) {
        this.context = context;
    }

    @Override
    public void process(Record<Void, String> record) {
        var gson = new Gson();
        RecordGenerator jsonObject = gson.fromJson(record.value(), RecordGenerator.class);
        System.out.println("Anonymizing record .... [cip: " + jsonObject.cip + "]");
        jsonObject.setLastName("***");
        jsonObject.setName("***");
        System.out.println("Received message: " + jsonObject.cip);
        String o = RecordGenerator.getJsonObject(jsonObject);
        // Lambda that returns a Record only if the condition is true
        if (jsonObject.price >= 4) {
            context.forward(record.withValue(o));
        };

    }

    @Override
    public void close() {
    }
}