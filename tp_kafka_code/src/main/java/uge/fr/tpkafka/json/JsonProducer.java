package uge.fr.tpkafka.json;

import org.apache.kafka.clients.producer.KafkaProducer;
import uge.fr.tpkafka.services.PostgresService;
import uge.fr.tpkafka.RecordGenerator;

import java.sql.SQLException;
import java.util.Properties;

import static uge.fr.tpkafka.RecordGenerator.getJsonObject;


/**
 * Simple Kafka Producer that produce json
 * Used to read data from Postgres and send it to Kafka Consumers
 */
public class JsonProducer {
    public static void main(String[] args) throws SQLException {
        String topic = "mytopic";
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        System.out.println("Producer created");
        // Create a PostgresService to connect to the database
        var bd = new PostgresService();
        // Loop to send data to Kafka
        for (int i = 0; i < 1500; i++) {
            var myRecord = new RecordGenerator(bd);
            String json = getJsonObject(myRecord);
            producer.send(new org.apache.kafka.clients.producer.ProducerRecord<>(topic, json));
            System.out.println("Record " + i + " sent successfully");
            System.out.println(json);
            System.out.println("-----------------------");
        }
        bd.close();

    }

}
