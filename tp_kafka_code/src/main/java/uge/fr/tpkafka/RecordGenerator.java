package uge.fr.tpkafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import uge.fr.tpkafka.services.PostgresService;

import java.sql.SQLException;
import java.util.Random;

/**
 * Class to generate a random record
 */

public class RecordGenerator   {

    public String name;
    public String lastName;
    public int cip;
    public Double price;
    public int pharmacyId;

    public RecordGenerator(PostgresService bd) throws SQLException {
        fakeClient();
        drugsData(bd);
        pharmacyData(bd);
    }

    public static String getJsonObject(RecordGenerator record) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(record);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    private void drugsData(PostgresService bd) throws SQLException {

        var res = bd.executeQuery("SELECT CIP, prix FROM drugs4Projet ORDER BY RANDOM() LIMIT 1;");
       res.next();
       this.cip = res.getInt("CIP");
       this.price = res.getDouble("prix");
       this.price =  price + price * (new Random().nextFloat() - 0.5) * 0.2;
       this.price = Math.round(price * 100.0)/100.0;
    }
    private void pharmacyData(PostgresService bd) throws SQLException {
        var res= bd.executeQuery("SELECT id, nom FROM pharm4projet ORDER BY RANDOM() LIMIT 1;");
        res.next();
        this.pharmacyId  = res.getInt("id");
    }
    private void fakeClient() {
        Faker faker = new Faker();
        name = faker.name().firstName();
        lastName = faker.name().lastName();

    }
    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCip(int cip) {
        this.cip = cip;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setPharmacyId(int pharmacyId) {
        this.pharmacyId = pharmacyId;
    }
    @Override
    public String toString() {
        return "RecordGenerator{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cip=" + cip +
                ", price=" + price +
                ", pharmacyId=" + pharmacyId +
                '}';
    }
}
