package uge.fr.tpkafka.services;

import java.sql.*;
import java.util.HashMap;

public class PostgresService {
   private final Statement stmt;
   private String username;
   private String password;

   public PostgresService(String username, String password) throws SQLException {
      this.username = username;
      this.password = password;
      this.stmt = DriverManager.getConnection(
              "jdbc:postgresql://localhost:5432/postgres",username,password).createStatement();
   }

   public PostgresService() throws SQLException {
      this.stmt = DriverManager.getConnection(
              "jdbc:postgresql://localhost:5432/postgres","vaahidra","Wahid.wahid06").createStatement();
   }

   public ResultSet executeQuery(String query) throws SQLException {
      return stmt.executeQuery(query);
   }


   public void close() throws SQLException {
      stmt.close();
   }



}
