[//]: # (Title : Compte rendu TP Kafka)
## Compte Rendu TP Kafka

### 1. Mise en place des serveurs Kafka en utilisant Docker
[//]: breakpoint
1.1 : J'ai créé un fichier docker-compose.yml pour lancer les 3 serveurs Kafka.
```yaml
version: "2"
services:
  zookeeper:
    image: wurstmeister/zookeeper
    ports:
      - "2181:2181"
  kafka1:
    image: wurstmeister/kafka
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka1:9092
      KAFKA_LISTENERS: PLAINTEXT://0.0.0.0:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_BROKER_ID: 0
      # Create 1 topic with 3 partitions and 3 replicas
      KAFKA_CREATE_TOPICS: "Top2:3:3"
    depends_on:
      - zookeeper

  kafka2:
    image: wurstmeister/kafka
    ports:
      - "9093:9093"
    environment:
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka2:9093
      KAFKA_LISTENERS: PLAINTEXT://0.0.0.0:9093
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_BROKER_ID: 1
    depends_on:
      - zookeeper
  kafka3:
    image: wurstmeister/kafka
    ports:
      - "9094:9094"
    environment:
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka3:9094
      KAFKA_LISTENERS: PLAINTEXT://0.0.0.0:9094
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_BROKER_ID: 2

    depends_on:
      - zookeeper
```
1.2 : Une fois les 3 serveurs Kafka lancés, topic Top2 est créé avec 3 partitions et 3 réplicas.
![image](/Users/vaahidra/Desktop/c1.png)

### 2. Lancement de Producer et Consumer Avro


Avant de lacer les class Java lancés, on peux voir que les partitions du topic Top2 est vide.
```bash
$ docker-compose exec kafka1 bash
$ kafka-run-class.sh kafka.tools.GetOffsetShell --broker-list localhost:9092 --topic Top2 --time -1

Top2:0:0
Top2:1:0
Top2:2:0
```
Une fois les class Java lancés, on peux voir que les partitions du topic Top2 est rempli et bien réparti.

J'ai tester avec une boucle de 1500 messages en veillant à bien passer CIP comme clé.
inserer une iamge ici
![image](/Users/vaahidra/Desktop/c2.png)

Avant de passer à la partie KStream, je voudrais mentionner que j'ai découvert ce joli 
que j'ai découvert malheureusment en retard, j'ai relancer avec 3000 Message, voila le résultat 
dans l'outil Offset Explorer 
![image](/Users/Vaahidra/Desktop/c4.png)

Le résultat d'execution sur le terminal:
![image](/Users/Vaahidra/Desktop/c5.png)


### 3. Utilisation de KStreams pour la transformation des données

#### Anonymisation des ventes & Filtrage

- J'ai réussi à concevoir la topologie de processeurs KStreams, j'ai pas mal galérer pour trouver comment faire, car
j'avais commencer sans utiliser la topologie de processeurs, j'ai essayer d'utiliser le DSL API, 
j'ai eu des erreurs de type mismatch, tellement j'essayer de initialiser mon KStream en utilisant mon object RecordGenerator

- J'ai donc switcher et utiliser l'API Processor, comme on peut le voir dans la classe MyProcessor, cela m'a permit de mieux comprendre
les schema présent dans le cours, notamment, l'histoire de addSource, addSink 

- J'ai tester les deux version, ça fonctionne bien, j'arrive à Anonymiser les ventes, et égalemenent 
appliquer un flitre sur le prix > 4 

- Comme vous nous aviez dis, que vous regarderai que le code, j'ai penser à vous faire un capture d'écran
de mon terminal, voila donc le resultat:
![image](/Users/vaahidra/Desktop/c6.png)

- Ici On peut voir de gauche droite 
L'envoi du premier message en Json, au Consumer Json, qui lui passe par un traitement KStream, pour que 
le consumer le plus adroite, puisse afficher les ventes anonymisées et filtrées.



### Conclusion

- J'ai bien aimé ce TP, j'ai appris beaucoup de chose, 
notamment comment manupiler, exploiter et utiliser Kafka, 
sous plusieurs angles, avec différentes manière,j'ai également 
vu comment utiliser Avro ce qui permet de bénéficier de la sérialisation et de la 
désérialisation des données efficacement et de faciliter l'échange de données entre les différents systèmes. 
Puis, j'ai pu voir comment utiliser KStream, pour transformer les données, et appliquer des filtres.
C'est tellement puissant, et en attente de votre retour j'espère pouvoir l'utiliser pour notre projet de JEE.






